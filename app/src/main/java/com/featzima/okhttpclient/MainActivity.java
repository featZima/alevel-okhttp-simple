package com.featzima.okhttpclient;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import dagger.android.support.DaggerAppCompatActivity;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends DaggerAppCompatActivity {

    @Inject
    OkHttpAsyncTask okHttpAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                okHttpAsyncTask.execute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class RepositoryEntity {
        String id;
        String full_name;
    }

    static class OkHttpAsyncTask extends AsyncTask<URI, Void, String> {

        private final OkHttpClient okHttpClient;

        @Inject
        public OkHttpAsyncTask(OkHttpClient okHttpClient) {
            this.okHttpClient = okHttpClient;
        }

        @Override
        protected String doInBackground(URI... uris) {
            Request request = new Request.Builder()
                    .get()
                    .url("https://api.github.com/user/repos")
                    .build();
            try {
                Call call = okHttpClient.newCall(request);
                Response response = call.execute();

                String jsonResponse = response.body().string();
                Gson gson = new Gson();
                Type listType = new TypeToken<List<RepositoryEntity>>() {
                }.getType();
                List<RepositoryEntity> repositories = gson.fromJson(jsonResponse, listType);
                for (RepositoryEntity repository : repositories) {
                    Log.e("!!!", repository.full_name);
                }
            } catch (IOException exception) {
                Log.e("!!!", exception.getMessage());
            }
            return null;
        }
    }
}
