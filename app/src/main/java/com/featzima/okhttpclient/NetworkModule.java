package com.featzima.okhttpclient;

import java.io.IOException;
import java.util.Set;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class NetworkModule {

    @Provides
    @IntoSet
    Interceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @IntoSet
    Interceptor providerAuthenticationInterceptor(AuthentificationInterceptor interceptor) {
        return interceptor;
    }

    @Provides
    CredentialsRepository provideCredentialsRepository(CredentialsRepositoryImpl repository) {
        return repository;
    }

    @Provides
    Authenticator provideAuthenticator(final CredentialsRepository credentialsRepository) {
        return new Authenticator() {

            @Override
            public Request authenticate(Route route, Response response) throws IOException {
                String credential = Credentials.basic(
                        credentialsRepository.getCredentials().getUsername(),
                        credentialsRepository.getCredentials().getPassword());
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }
        };
    }

    @Provides
    OkHttpClient provideOkHttpClient(Set<Interceptor> interceptors, Authenticator authenticator) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        for (Interceptor interceptor : interceptors) {
            builder.addInterceptor(interceptor);
        }
        builder.authenticator(authenticator);
        OkHttpClient client = builder.build();
        return client;
    }

}
