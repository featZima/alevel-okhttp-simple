package com.featzima.okhttpclient;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class AuthentificationInterceptor implements Interceptor {

    private final CredentialsRepository credentialsRepository;

    @Inject
    public AuthentificationInterceptor(CredentialsRepository credentialsRepository) {
        this.credentialsRepository = credentialsRepository;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String credential = Credentials.basic(
                credentialsRepository.getCredentials().getUsername(),
                credentialsRepository.getCredentials().getPassword());
        Request request;
        if (!chain.request().url().toString().contains("/profile")) {
            request = chain.request().newBuilder()
                    .header("Authorization", credential)
                    .build();
        } else {
            request = chain.request();
        }
        Response response = chain.proceed(request);
        return response;
    }
}
