package com.featzima.okhttpclient;

public interface CredentialsRepository {

    Credentials getCredentials();

}
