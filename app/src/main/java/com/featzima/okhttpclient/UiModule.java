package com.featzima.okhttpclient;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
public interface UiModule {

    @ContributesAndroidInjector
    MainActivity mainActivity();


}
