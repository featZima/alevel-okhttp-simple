package com.featzima.okhttpclient;


import android.content.Context;

import dagger.Component;
import dagger.android.AndroidInjector;

@Component(modules = {
        NetworkModule.class,
        UiModule.class
})
public interface ApplicationComponent extends AndroidInjector<Application> {

    void inject(MainActivity activity);

    @Component.Builder
    interface Builder {

        ApplicationComponent build();
    }

}
